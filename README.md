# Run time test suite for GNU Guix

This simple test suite written in bash is intended to verify [GNU Guix](https://www.gnu.org/software/guix/) at
run time. It also verifies if Guix is correctly installed.

## Run test suite

Just run from root folder of this repository:
```bash
./run-tests.sh
```

If you want to run a particular test you can call it directly with:
```bash
./tests/NAME.sh
```
