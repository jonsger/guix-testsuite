#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
check "$cmd --search=hello"
check "$cmd --show=hello"
check "$cmd --install=hello"
check "hello"
check "$cmd --remove=hello"
check_fail "hello"
check_fail "$cmd --lph"
check_fail "$cmd --show=$no_package"
check "$cmd -A hello"
check "$cmd --list-generations"
