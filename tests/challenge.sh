#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
# needed for the challenge
check "guix build --check hello"
check "$cmd hello"
