#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
export VISUAL=cat
check "$cmd hello"
unset $VISUAL
