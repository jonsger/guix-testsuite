#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
check "$cmd --list-checkers"
# cve checker is very slow, so only use some local and remote checkers
check "$cmd --checkers=description,inputs-should-be-native,license,mirror-url,home-page,refresh hello"
# Reproducer for https://issues.guix.gnu.org/issue/37160
check "$cmd --checkers=refresh hello"
check_fail "$cmd --checkers=description $no_package"
