#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
# guix describe works only when guix is installed via guix pull
check_fail "$cmd"
