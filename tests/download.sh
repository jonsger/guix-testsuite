#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
check "$cmd https://www.gnu.org"
check_fail "$cmd https://www.gnu.org/"
