#!/bin/bash
source functions.sh
cmd="guix $(basename $0 .sh)"

check "$cmd --help"
check "$cmd --pure --ad-hoc hello -- hello"
check_fail "$cmd --pure --ad-hoc hello -- $no_package"
