#!/bin/bash
# Global variables required
no_package="hurzelpurz"

# Functions required for the tests
function check {
  $1 > .log.txt 2>&1; status=$?
  if [ $status -eq 0 ]; then
    echo "[PASS]: $1"
  else
    echo "[FAIL]: $1"
    cat .log.txt
  fi
}

function check_fail {
  $1 > .log.txt 2>&1; status=$?
  if [ $status -ne 0 ]; then
    echo "[PASF]: $1"
  else
    echo "[FAIL]: $1"
    cat .log.txt
  fi
}
