#!/bin/bash
# Required environment variables
# guix install nss-certs
export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
export GIT_SSL_CAINFO="$SSL_CERT_FILE"

# Script will run all tests in tests/
for s in tests/*.sh; do
  bash "$s"
done

# Clean-up
rm .log.txt
